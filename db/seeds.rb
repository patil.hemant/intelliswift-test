# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

category_1 = Category.create(:name => 'Accessories')
category_2 = Category.create(:name => 'Clothes')
category_3 = Category.create(:name => 'Shoes')


Product.create(:title => "Wallet", :description => "It's a wallet made up of leather material", :category_id => category_1.id)
Product.create(:title => "Shirt", :description => "It's a branded shirt of peter england quality", :category_id => category_2.id)
Product.create(:title => "Jeans", :description => "It's a branded shirt of venhuesen quality", :category_id => category_2.id)
Product.create(:title => "Bangle", :description => "It's a from maharashtrian ethnic", :category_id => category_1.id)
Product.create(:title => "Casual shoes", :description => "It's a casual shoes", :category_id => category_3.id)
Product.create(:title => "Kolhapuri chappal", :description => "It's a Kolhapuri chappal from maharashtrian ethnic", :category_id => category_3.id)