class ProductsController < ApplicationController
	def index
		if params[:c_id].present?
			@category = Category.find_by_id(params[:c_id])
			if @category.present?
				@products = @category.products
			end
		end
	end

	def get_total_likes_day_wise
	end

	def show
		@product = Product.find(params[:id])
	end
end
