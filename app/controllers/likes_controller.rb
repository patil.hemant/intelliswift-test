class LikesController < ApplicationController
	
	before_action :authenticate_user!

	def create
		@like = Like.create_record(params)
	end


	def destroy
		like = Like.find_by_id(params[:id])
		like.destroy
		render "likes/create.js"
	end

end
