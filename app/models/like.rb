class Like < ActiveRecord::Base
	
	belongs_to :likeable, :polymorphic => true

	after_create :increament_likes_count
	before_destroy :decrement_likes_count

	def increament_likes_count
		self.likeable.update_column('likes_count', self.likeable.likes_count + 1)
	end

	def decrement_likes_count		
		self.likeable.update_column('likes_count', self.likeable.likes_count == 1 ? 0 : self.likeable.likes_count - 1)
	end

	def self.create_record(params)
		Like.create(:liker_id => params[:liker_id], :likeable_id => params[:likeable_id], :likeable_type => params[:likeable_type])
	end

	def self.get_record(likeable_id, likeable_type, user_id)
		Like.where(:liker_id => user_id, :likeable_id => likeable_id, :likeable_type => likeable_type).last
	end
end
