class Product < ActiveRecord::Base
	belongs_to :category
	has_many :likes, :as => :likeable, :dependent => :destroy
end
